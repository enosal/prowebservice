package edu.uchicago.enosal.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Collection;

/**
 * Artist class
 * Created by Eryka on 8/20/2016.
 */
@Entity
public class Artist {
    ///////MEMBERS///////
    @Id
    @GeneratedValue
    private Long id;

    @Column(name="name")
    @NotEmpty
    private String name;

    @Column(name = "desc")
    private String desc;

    ///////CONSTRUCTORS///////

    public Artist(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public Artist() {

    }


    ///////METHODS///////

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
