package edu.uchicago.enosal.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Painting Class
 * Created by Eryka on 8/20/2016.
 */
@Entity
public class Painting {
    ///////MEMBERS///////
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    @NotEmpty
    private String name;

    @Column(name = "imgurl")
    @NotEmpty
    private String imgurl;

    @ManyToOne
    private Artist artist;

    ///////CONSTRUCTORS///////
    public Painting(String name, Artist artist, String imgurl) {
        this.name = name;
        this.artist = artist;
        this.imgurl = imgurl;
    }

    public Painting() {}


    ///////METHODS///////


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(String artist) {         this.artist.setName(artist);    }

    public void setArtist(Artist artist) {this.artist= artist;}


    @Override
    public String toString() {
        return "Painting{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imgurl='" + imgurl + '\'' +
                ", artist=" + artist +
                '}';
    }
}
