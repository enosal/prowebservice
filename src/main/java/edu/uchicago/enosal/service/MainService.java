package edu.uchicago.enosal.service;

import edu.uchicago.enosal.domain.Artist;
import edu.uchicago.enosal.domain.Painting;

/**
 * Interface for the main services for the website
 * Created by Eryka on 8/21/2016.
 */
public interface MainService {

    Iterable<Painting> listPaintings();

    Painting createPainting(Painting painting);

    Painting readPainting(long id);

    Painting updatePainting(long id, Painting painting);

    void deletePainting(long id);

    Iterable<Artist> listArtists();

    Artist createArtist(Artist artist);

    Artist readArtist(long id);

    Artist updateArtist(long id, Artist artist);
}
