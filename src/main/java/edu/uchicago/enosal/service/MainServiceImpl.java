package edu.uchicago.enosal.service;

import edu.uchicago.enosal.domain.Artist;
import edu.uchicago.enosal.domain.Painting;
import edu.uchicago.enosal.respository.ArtistRepository;
import edu.uchicago.enosal.respository.PaintingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implements Main Service
 * Created by Eryka on 8/21/2016.
 */
@Service
public class MainServiceImpl implements MainService {

    private PaintingRepository paintingRepo;
    private ArtistRepository artistRepo;

    @Autowired
    public MainServiceImpl(PaintingRepository paintingRepo, ArtistRepository artistRepo) {
        this.paintingRepo = paintingRepo;
        this.artistRepo = artistRepo;
    }

    @Override
    public Iterable<Painting> listPaintings() {
        return paintingRepo.findAll();
    }

    @Override
    public Painting createPainting(Painting painting) {
        return paintingRepo.save(painting);
    }

    @Override
    public Painting readPainting(long id) {
        return paintingRepo.findOne(id);
    }

    @Override
    public Painting updatePainting(long id, Painting painting) {
        Painting paintingInitial = paintingRepo.findOne(id);
        paintingInitial.setName(painting.getName());
        paintingInitial.setImgurl(painting.getImgurl());
        return paintingRepo.save(paintingInitial);

    }

    @Override
    public void deletePainting(long id) {
        paintingRepo.delete(id);
    }

    @Override
    public Iterable<Artist> listArtists() {
        return artistRepo.findAll();
    }

    @Override
    public Artist createArtist(Artist artist) {
        return artistRepo.save(artist);
    }

    @Override
    public Artist readArtist(long id) {
        return artistRepo.findOne(id);
    }

    @Override
    public Artist updateArtist(long id, Artist artist) {
        Artist artistInitial = artistRepo.findOne(id);
        artistInitial.setName(artist.getName());
        artistInitial.setDesc(artist.getDesc());
        return artistRepo.save(artistInitial);

    }
}
