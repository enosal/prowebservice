package edu.uchicago.enosal.respository;

import edu.uchicago.enosal.domain.Painting;
import org.springframework.data.repository.CrudRepository;

/**
 * Painting repository
 * Created by Eryka on 8/20/2016.
 */
public interface PaintingRepository extends CrudRepository<Painting, Long> {
}
