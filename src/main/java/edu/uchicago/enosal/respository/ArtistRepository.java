package edu.uchicago.enosal.respository;

import edu.uchicago.enosal.domain.Artist;
import org.springframework.data.repository.CrudRepository;

/**
 * Artist repository
 * Created by Eryka on 8/20/2016.
 */
public interface ArtistRepository extends CrudRepository<Artist, Long> {
}
