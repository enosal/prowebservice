package edu.uchicago.enosal.controller;

import edu.uchicago.enosal.domain.Artist;
import edu.uchicago.enosal.domain.Painting;
import edu.uchicago.enosal.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller that naps methods to HTTP verbs
 * Created by Eryka on 8/21/2016.
 */
@Controller
public class MainController {
    private MainService mainService;

    @Autowired
    public MainController(MainService mainServ) {
        this.mainService = mainServ;
    }

    //Disallow changes to id
    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    ////////////////
    // PAINTINGS //
    //////////////

    //GET. List of paintings
    @RequestMapping(value="/paintings", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("paintings", mainService.listPaintings());
        return "paintings/paintings";
    }

    //GET. Read a single painting
    @RequestMapping(value = "/paintings/{id}", method = RequestMethod.GET)
    public String read(@PathVariable(value = "id") long id, Model model) {
        Painting painting = mainService.readPainting(id);
        model.addAttribute("painting", mainService.readPainting(id));
        model.addAttribute("artist", painting.getArtist());
        return "paintings/viewpaintings";
    }

    //GET. View edit page
    @RequestMapping(value="/paintings/{id}/edit", method = RequestMethod.GET)
    public String editForm(@PathVariable(value = "id") long id, Model model) {
        Painting painting = mainService.readPainting(id);
        model.addAttribute("painting", mainService.readPainting(id));
        model.addAttribute("artist", painting.getArtist());
        return "paintings/editpaintings";
    }

    //PUT/POST. Update a page
    @RequestMapping(value = "/paintings/{id}/edit", method = RequestMethod.POST)
    public String update(@PathVariable(value = "id") long id, @Valid Painting painting, Model model) {
        Painting updatedPainting = mainService.updatePainting(id, painting);
        model.addAttribute("painting", mainService.readPainting(updatedPainting.getId()));
        model.addAttribute("artist", painting.getArtist());
        return "redirect:/paintings/" + updatedPainting.getId();
    }

    //DELETE. Delete
    @RequestMapping(value = "/paintings/{id}/delete")
    public String delete(@PathVariable(value = "id") long id) {
        mainService.deletePainting(id);
        return "redirect:/paintings/";
    }

    //GET. Retrieves new form page
    @RequestMapping(value="/paintings/new", method=RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("painting", new Painting());
        model.addAttribute("artists", mainService.listArtists());
        return "paintings/newpainting";
    }

    //POST. Saves new entry
    @RequestMapping(value="/paintings", method = RequestMethod.POST)
    public String save(@Valid Painting painting, BindingResult result) {
        if (result.hasErrors()) {
            return "newpainting";
        } else {
            mainService.createPainting(painting);
            return "redirect:/paintings/" + painting.getId();
        }
    }



    //////////////
    // ARTISTS //
    ////////////

    //GET. List of paintings
    @RequestMapping(value="/artists", method = RequestMethod.GET)
    public String listArtists(Model model) {
        model.addAttribute("artists", mainService.listArtists());
        return "artists/artists";
    }

    //GET. Read a single painting
    @RequestMapping(value = "/artists/{id}", method = RequestMethod.GET)
    public String readArtists(@PathVariable(value = "id") long id, Model model) {
        Artist artist = mainService.readArtist(id);
        model.addAttribute("artist", mainService.readArtist(id));
        return "artists/viewartists";
    }

    //GET. View edit page
    @RequestMapping(value="/artists/{id}/edit", method = RequestMethod.GET)
    public String editFormArtists(@PathVariable(value = "id") long id, Model model) {
        Painting painting = mainService.readPainting(id);
        model.addAttribute("artist", mainService.readArtist(id));
        return "artists/editartists";
    }

    //PUT/POST. Update a page
    @RequestMapping(value = "/artists/{id}/edit", method = RequestMethod.POST)
    public String updateArtist(@PathVariable(value = "id") long id, @Valid Artist artist, Model model) {
        Artist updatedArtist = mainService.updateArtist(id, artist);
        model.addAttribute("artist", mainService.readArtist(updatedArtist.getId()));
        return "redirect:/artists/" + updatedArtist.getId();
    }


    //GET. Retrieves new form page
    @RequestMapping(value="/artists/new", method=RequestMethod.GET)
    public String createArtist(Model model) {
        model.addAttribute("artist", new Artist());
        return "artists/newartist";
    }

    //POST. Saves new entry
    @RequestMapping(value="/artists", method = RequestMethod.POST)
    public String saveArtist(@Valid Artist artist, BindingResult result) {
        if (result.hasErrors()) {
            return "artists/newartist";
        } else {
            mainService.createArtist(artist);
            return "redirect:/artists/" + artist.getId();
        }
    }




}
