package edu.uchicago.enosal.controller;


import edu.uchicago.enosal.domain.Painting;
import edu.uchicago.enosal.respository.PaintingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest Controller
 * Reference: http://kubecloud.io/guide-getting-started-with-spring-boot/
 * Created by Eryka on 8/24/2016.
 */
@RestController
@RequestMapping("/api")
public class PaintingRestController {

    @Autowired
    private PaintingRepository paintingRepo;

    //GET. All paintings
    @RequestMapping(value = "getPaintings", method = RequestMethod.GET)
    public ResponseEntity<List<Painting>> getPaintings() {
        return new ResponseEntity<>((List<Painting>) paintingRepo.findAll(), HttpStatus.OK);
    }

    //List
    @RequestMapping(value = "getPaintings/{id}", method = RequestMethod.GET)
    public ResponseEntity<Painting> getPainting(@PathVariable long id) {
        return new ResponseEntity<>(paintingRepo.findOne(id), HttpStatus.OK);
    }

    //CREATE
    @RequestMapping(value= "getPaintings", method=RequestMethod.POST)
    public ResponseEntity<Painting> create(@RequestBody Painting painting) {
        return new ResponseEntity<>(paintingRepo.save(painting), HttpStatus.CREATED);
    }

    //UPDATE.
    @RequestMapping(value="getPaintings/{id}/edit", method =RequestMethod.PUT)
    public ResponseEntity<Painting> painting(@PathVariable long id, @RequestBody Painting painting) {
        Painting paintingInitial = paintingRepo.findOne(id);
        paintingInitial.setName(painting.getName());
        paintingInitial.setImgurl(painting.getImgurl());
        return new ResponseEntity<>(paintingRepo.save(paintingInitial), HttpStatus.OK);
    }

    //DELETE
    @RequestMapping(value = "getPaintings/{id}/delete")
    public void delete(@PathVariable long id) {
        paintingRepo.delete(id);
    }


}
