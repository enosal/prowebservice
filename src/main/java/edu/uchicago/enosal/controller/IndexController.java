package edu.uchicago.enosal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for home and about methods
 * Created by Eryka on 8/23/2016.
 */
@Controller
public class IndexController {
    //Home
    @RequestMapping(value="/", method = RequestMethod.GET)
    public String index() {
        return "index"; //.html
    }

    //About
    @RequestMapping(value="/about", method = RequestMethod.GET)
    public String about() {
        return "about"; //.html
    }

}
