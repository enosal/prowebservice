package edu.uchicago.enosal;

import edu.uchicago.enosal.domain.Artist;
import edu.uchicago.enosal.domain.Painting;
import edu.uchicago.enosal.respository.ArtistRepository;
import edu.uchicago.enosal.respository.PaintingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class ProWebserviceApplication implements CommandLineRunner {

	@Autowired
	private ArtistRepository artistRepo;

	@Autowired
	private PaintingRepository paintingRepo;

	public static void main(String[] args) {
		SpringApplication.run(ProWebserviceApplication.class, args);
	}

    @Override
    public void run(String... strings) throws Exception {

        Artist[] artists = new Artist[5];
        artists[0] = new Artist("Claude Monet", "Oscar-Claude Monet (14 November 1840 – 5 December 1926) was a founder of French Impressionist painting.");
        artists[1] = new Artist("Armand Guillamin", "Armand Guillaumin (February 16, 1841 – June 26, 1927) was a French impressionist painter and lithographer.");
        artists[2] = new Artist("Paul Signac", "Paul Victor Jules Signac (11 November 1863 – 15 August 1935) was a French Neo-Impressionist painter who, working with Georges Seurat, helped develop the Pointillist style.");
        artists[3] = new Artist("Henri Edmond-Cross", "Henri-Edmond Cross, born Henri-Edmond-Joseph Delacroix, (20 May 1856 – 16 May 1910) is most acclaimed as a master of Neo-Impressionism.");
        artists[4] = new Artist("Theo van Rysselberghe", "Théophile van Rysselberghe (23 November 1862 – 14 December 1926) was a Belgian neo-impressionist painter, who played a pivotal role in the European art scene at the turn of the century.");

        artistRepo.save(Arrays.asList(artists));

        Painting[] paintings = new Painting[17];
        paintings[0] = new Painting("San Giorgio Maggiore at Dusk", artists[0], "http://www.claudemonetgallery.org/San-Giorgio-Maggiore-At-Dusk.jpg");
        paintings[1] = new Painting("The Artist's Garden at Giverny", artists[0], "http://www.claudemonetgallery.org/The-Artist's-Garden-at-Giverny.jpg");
        paintings[2] = new Painting("Morning on the Seine IV", artists[0], "http://www.claudemonetgallery.org/Morning-on-the-Seine-IV.jpg");
        paintings[3] = new Painting("Arm of the Seine near Giverny at Sunrise", artists[0], "http://www.claudemonetgallery.org/Arm-Of-The-Seine-Near-Giverny-At-Sunrise.jpg");
        paintings[4] = new Painting("The Japanese Bridge", artists[0], "http://www.claudemonetgallery.org/download-81596-water-lily-pond--water-irises.download");
        paintings[5] = new Painting("L'Arbre-En-Boule, Argenteuil", artists[0], "http://www.claudemonetgallery.org/L'Arbre-En-Boule,-Argenteuil.jpg");
        paintings[6] = new Painting("Grainstacks", artists[0], "http://www.claudemonetgallery.org/Grainstacks.jpg");
        paintings[7] = new Painting("The Pine Tree at St. Tropez", artists[2], "http://www.paul-signac.org/The-Pine-Tree-at-St.-Tropez,-1909.jpg");
        paintings[8] = new Painting("St. Tropez, Pinewood", artists[2], "http://www.paul-signac.org/St.-Tropez,-Pinewood,-1896.jpg");
        paintings[9] = new Painting("Forest Near St. Tropez", artists[2], "http://www.paul-signac.org/Forest-near-St.-Tropez,-1902.html");
        paintings[10] = new Painting("Saint Cloud", artists[2], "http://www.paul-signac.org/Saint-Cloud.jpg");
        paintings[11] = new Painting("St. Tropez", artists[2], "http://www.paul-signac.org/Saint-Tropez.jpg");
        paintings[12] = new Painting("The Chateau des Papes", artists[2], "http://www.paul-signac.org/The-Chateau-des-Papes.jpg");
        paintings[13] = new Painting("Terrasse de Meduon", artists[2], "http://www.paul-signac.org/Terrasse-De-Meudon.jpg");
        paintings[14] = new Painting("La Plage de Saint-Clair", artists[3], "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Henri_Edmond_Cross_La_Plage_de_Saint-Clair.jpg/1024px-Henri_Edmond_Cross_La_Plage_de_Saint-Clair.jpg");
        paintings[15] = new Painting("The Cypresses at Cagnes", artists[3], "http://uploads0.wikiart.org/images/henri-edmond-cross/the-cypresses-at-cagnes-1908.jpg!Large.jpg");
        paintings[16] = new Painting("The Pink Cloud", artists[3], "http://uploads4.wikiart.org/images/henri-edmond-cross/the-pink-cloud.jpg!Large.jpg");
//        paintings[18] = new Painting("The Port, Red Sunset", new Artist("Lone Quixote", "", ""), "http://www.lonequixote.com/uploads/1/3/3/9/13399473/5017034_orig.jpg");

        paintingRepo.save(Arrays.asList(paintings));




    }
}
