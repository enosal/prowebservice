proWebService
=============

This is a Spring Boot application that displays artwork by impressionist painters.

Start: localhost:8080/

Notes:
*Some artwork may be slow to load
*The search bar does not work; it's just there for the aesthetic

Advanced Java, Summer 2016